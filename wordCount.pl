
# coding: utf-8

# In[28]:



import ibmos2spark

# @hidden_cell
credentials = {
    'auth_url': 'https://lon-identity.open.softlayer.com',
    'project_id': 'c930419a5511473cacf4d68712c07df4',
    'region': 'london',
    'user_id': 'cd258443b3954b548d5d9711c066f3e1',
    'username': 'member_3d2ce85b3670c89f1900150ab1b66817c6b0b9c0',
    'password': 'gM[W(38vp.xaZ0~}'
}

configuration_name = 'os_0c7790547a00426da1258696c237d6ef_configs'
bmos = ibmos2spark.bluemix(sc, credentials, configuration_name)

from pyspark.sql import SparkSession
spark = SparkSession.builder.getOrCreate()
# Please read the documentation of PySpark to learn more about the possibilities to load data files.
# PySpark documentation: https://spark.apache.org/docs/2.0.1/api/python/pyspark.sql.html#pyspark.sql.SparkSession
# The SparkSession object is already initalized for you.
# The following variable contains the path to your file on your Object Storage.
path_1 = bmos.url('Assignment3', 'WordCountData.txt')


# In[244]:


#does not make use of lemmatization or stemming
import re
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.corpus import stopwords

def countWords():
    textFile = sc.textFile(path_1)
    data = textFile.flatMap(lambda line: re.sub(r'[^a-zA-Z\s]+','',line).split())
    wordCounts = data.map(lambda word: (word.lower(), 1)).reduceByKey(lambda v1,v2:v1 +v2)
    filteredWordCount = wordCounts.count()
    print "Total Number unique Words:"+str(filteredWordCount)
    wordFreq = wordCounts.collect()
    print "Word Frequencies\n"
    for elem in wordFreq:
        print elem
    return 0


# In[245]:


countWords()


# In[225]:


#Makes use of lemmatization in calculating the number of unique words and their frequency
import re
import nltk
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer

lemma = WordNetLemmatizer()   

def countWord():
    textFile = sc.textFile(path_1)
    data = textFile.flatMap(lambda line: word_tokenize(re.sub(r'[^a-zA-Z\s]+','',line).lower().strip()))
    values = data.collect()
    wordArray = []
    stopWords = set(stopwords.words('english'))
    
    for value in values:
        lemmatizedValue = lemma.lemmatize(value)
        if not lemmatizedValue in stopWords:
            wordArray.append(lemmatizedValue)
    rdd = spark.sparkContext.parallelize(wordArray)
    wordDis = rdd.map(lambda x : (x,1)).reduceByKey(lambda v1, v2 : v1+v2)
    print "Total Number of distinct words:"+str(wordDis.distinct().count())+"\n"
    print "Word Frequencies\n"
    wordFreq = wordDis.collect()
    for elem in wordFreq:
        print elem
    return 0


# In[231]:


countWord()


# In[227]:


#makin =g use of SnowBall stemmer to do the same thing
import re
import nltk
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.corpus import stopwords
from nltk.stem import SnowballStemmer

stemmer = SnowballStemmer("english")

def countWordStems():
    textFile = sc.textFile(path_1)
    data = textFile.flatMap(lambda line: word_tokenize(re.sub(r'[^a-zA-Z\s]+','',line).lower().strip()))
    values = data.collect()
    wordArray = []
    stopWords = set(stopwords.words('english'))
    
    for value in values:
        stemmedValue = stemmer.stem(value)
        if not stemmedValue in stopWords:
            wordArray.append(stemmedValue)
    rdd = spark.sparkContext.parallelize(wordArray)
    wordDis = rdd.map(lambda x : (x,1)).reduceByKey(lambda v1, v2 : v1+v2)
    print "Total Number of distinct words:"+str(wordDis.distinct().count())+"\n"
    print "Word Frequencies\n"
    wordFreq = wordDis.collect()
    for elem in wordFreq:
        print elem
    return 0


# In[232]:


countWordStems()

