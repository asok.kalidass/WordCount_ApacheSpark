# coding: utf-8

# In[1]:



import ibmos2spark

# @hidden_cell
credentials = {
    'endpoint': 'https://s3-api.us-geo.objectstorage.service.networklayer.com',
    'api_key': 'OLfVDvS_1OyHfzmwe5fJ7E43Z3qrOvPEak6V5xU6tOcv',
    'service_id': 'iam-ServiceId-188c8616-f0e7-4dca-9046-4440a698e58c',
    'iam_service_endpoint': 'https://iam.ng.bluemix.net/oidc/token'}

configuration_name = 'os_692f887ed348422881c28773c7edcf40_configs'
cos = ibmos2spark.CloudObjectStorage(sc, credentials, configuration_name, 'bluemix_cos')

from pyspark.sql import SparkSession
spark = SparkSession.builder.getOrCreate()
df_data_1 = spark.read  .format('org.apache.spark.sql.execution.datasources.csv.CSVFileFormat').option('header', 'true').load(cos.url('Collision_Statistics_Monthly_2000-2003.csv', 'assignment3fbcd80caa6c34535b952ac4c1afec9b5'))
df_data_1.take(5)


# In[5]:


#Capture total incident counts(including Fatal, Injury and Property Damage)in a year(grouped by year).

df_data_1.groupby("Year").agg({"Total": "SUM"}) .show()


# In[6]:


# Capture sum of injuries in Nova Scotiagrouped by year

df_data_1.filter(df_data_1["GeogName"] == "Nova Scotia").groupby("Year").agg({"Injury": "SUM"}) .show()


# In[7]:


# Capture sum of injury in Yarmouth Country in the year of 2000

df_data_1.filter(df_data_1["GeogName"] == "Yarmouth County").filter(df_data_1["Year"] == 2000).agg({"Injury": "SUM"}) .show()


# In[9]:


df_data_1.groupBy('Year').agg({"Total":"SUM"}).withColumnRenamed("sum(Total)","Total_Incident_Count").show()

